﻿Shader "Custom/Gradient" {
	Properties {
		_LeftColor ("L color", Color) = (1,1,1,1)
		_RightColor ("R color", Color) = (0, 0, 0, 1)
		_Center ("Center", Range(0, 1)) = .5
		_Band ("Band", Range(0, 1)) = .1
	}

	SubShader {
		Tags {
		}	// Tags {
		Cull Off

		Pass {
CGPROGRAM
	#include "UnityCG.cginc"
	#pragma vertex vert
	#pragma fragment frag
	#pragma target 3.0

	float4 _LeftColor;
	float4 _RightColor;
	float _Center;
	float _Band;

	struct VertexInput {
		float4 vertex : POSITION;
		float2 texcoord0 : TEXCOORD0;
	};

	struct Vert2Frag {
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD1;
	};

	Vert2Frag vert(VertexInput vertIn) {
		Vert2Frag output;
		output.pos = UnityObjectToClipPos(vertIn.vertex);
		output.uv = vertIn.texcoord0;

		return output;
	}

	float4 frag(Vert2Frag fragIn) : SV_Target {
		float hband = _Band * .5;
		float leftBorder = _Center - hband;
		float rightBorder = _Center + hband;
		if (fragIn.uv.x < leftBorder) {
			return _LeftColor;
		}
		else if (fragIn.uv.x > rightBorder) {
			return _RightColor;
		}
		else {
			float t = (fragIn.uv.x - leftBorder) / (rightBorder - leftBorder);
			return (1 - t) * _LeftColor + t * _RightColor;
		}
	}


ENDCG
		}	// Pass {
	}	// SubShader {
}	// Shader "Custom/Gradient" {