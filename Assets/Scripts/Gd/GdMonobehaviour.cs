﻿using System;
using System.Reflection;
using UnityEngine;
using Object = UnityEngine.Object;

public static class GdTransform {
    public static Vector2
    GetPos2d(this Transform transform) {
        return (Vector2)transform.position;
    }

    public static void
    SetPositionX(this Transform transform, float x) {
        Vector3 newPos = new Vector3(x, transform.position.y, transform.position.z);
        transform.position = newPos;
    }

    public static void
    SetPositionY(this Transform transform, float y) {
        Vector3 newPos = new Vector3(transform.position.x, y, transform.position.z);
        transform.position = newPos;
    }

    public static void
    SetPositionZ(this Transform transform, float z) {
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y, z);
        transform.position = newPos;
    }

    public static void
    SetLocalPosX(this Transform transform, float x) {
        Vector3 newPos = transform.localPosition;
        newPos.x = x;
        transform.localPosition = newPos;
    }

    public static void
    SetLocalPosY(this Transform transform, float y) {
        Vector3 newPos = transform.localPosition;
        newPos.y = y;
        transform.localPosition = newPos;
    }

    public static void
    SetLocalPosZ(this Transform transform, float z) {
        Vector3 newPos = transform.localPosition;
        newPos.z = z;
        transform.localPosition = newPos;
    }

    public static void
    SetScaleX(this Transform transform, float x) {
        Vector3 newScale = new Vector3(x, transform.localScale.y, transform.localScale.z);
        transform.localScale = newScale;
    }

    public static void
    SetScaleY(this Transform transform, float y) {
        Vector3 newScale = new Vector3(transform.localScale.x, y, transform.localScale.z);
        transform.localScale = newScale;
    }

    public static void
    SetScaleZ(this Transform transform, float z) {
        Vector3 newScale = new Vector3(transform.localScale.y, transform.localScale.y, z);
        transform.localScale = newScale;
    }

    public static void
    RotateWithDir(this Transform transform, Vector2 dir) {
        RotateZ(transform, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg);
    }

    public static void
    RotateZ(this Transform transform, float degZ) {
        transform.rotation = Quaternion.Euler(0, 0, degZ);
    }
}

public static class GdSpriteRenderer {
    public static void
    SetColorA(this SpriteRenderer rendSpr, float a) {
        Color c = rendSpr.color;
        c.a = a;
        rendSpr.color = c;
    }
    
    public static void
    SetColorR(this SpriteRenderer rendSpr, float r) {
        Color c = rendSpr.color;
        c.r = r;
        rendSpr.color = c;
    }

    public static void
    SetColorG(this SpriteRenderer rendSpr, float g) {
        Color c = rendSpr.color;
        c.g = g;
        rendSpr.color = c;
    }

    public static void
    SetColorB(this SpriteRenderer rendSpr, float b) {
        Color c = rendSpr.color;
        c.b = b;
        rendSpr.color = c;
    }
}

public class GdMonoBehaviour : MonoBehaviour {
    protected static readonly Vector3 v3zero = Vector3.zero;
    protected static readonly Vector3 v3one = Vector3.one;
    protected static readonly Vector3 v3forward = Vector3.forward;
    protected static readonly Vector3 v3back = Vector3.back;
    protected static readonly Vector3 v3right = Vector3.right;
    protected static readonly Vector3 v3left = Vector3.left;
    protected static readonly Vector3 v3up = Vector3.up;
    protected static readonly Vector3 v3down = Vector3.down;
    protected static readonly Vector2 v2zero = Vector2.zero;
    protected static readonly Vector2 v2one = Vector2.one;
    protected static readonly Vector2 v2right = Vector2.right;
    protected static readonly Vector2 v2left = Vector2.left;
    protected static readonly Vector2 v2up = Vector2.up;
    protected static readonly Vector2 v2down = Vector2.down;
    protected static readonly Quaternion quatIdentity = Quaternion.identity;

    protected GameObject go = null;
    protected Transform tr = null;

    void
    Awake() {
        go = gameObject;
        tr = transform;
        OnAwake();
    }

    virtual protected void OnAwake() {}

    public static GameObject
    InstantiateGameObject(Object obj) {
        GameObject go = Instantiate(obj) as GameObject;

        if (go == null) {
            Debug.LogError("Error...");
        }

        return go;
    }

    public static GameObject
    InstantiateGameObject(Object obj, Vector3 pos) {
        GameObject go = InstantiateGameObject(obj);
        go.transform.position = pos;
        return go;
    }

    public GameObject
    InstantiateGameObjectAsChild(Object obj) {
        GameObject go = InstantiateGameObject(obj);
//        go.transform.parent = transform;
        go.transform.SetParent(transform);
        go.transform.localPosition = Vector3.zero;
        return go;
    }

    public GameObject
    InstantiateGameObjectAsChildUI(Object obj, GameObject uiParent) {
        GameObject go = InstantiateGameObject(obj);
        go.transform.SetParent(uiParent.transform);
        go.transform.localPosition = Vector2.zero;
        go.transform.localScale = Vector2.one;
        return go;
    }
}

public static class GdGameObject {
    public static
    T
    GetCopyOf<T>(this Component comp, T other)
    where T : Component {
        Type type = comp.GetType();
        if (type != other.GetType()) {
            return null; // type mis-match
        }
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos) {
            if (pinfo.CanWrite) {
                try {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos) {
            finfo.SetValue(comp, finfo.GetValue(other));
        }  
        return comp as T;
    }

    public static
    T
    AddComponent<T>(this GameObject go, T toAdd) where T : Component {
        return go.AddComponent<T>().GetCopyOf(toAdd) as T;
    }
}