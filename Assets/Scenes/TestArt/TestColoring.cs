﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TestColoring : GdMonoBehaviour {
	[Header("ref")]
	[SerializeField] Coloring coloring;
	[SerializeField] SpriteRenderer[] planets;
	[SerializeField] SpriteRenderer bg;
	[SerializeField] ParticleSystem effMovingStars;

	void Start() {
#if !UNITY_EDITOR
		SetupColors();
#endif		
	}

	void Update () {
#if UNITY_EDITOR
		SetupColors();
#endif		
	}

	void SetupColors() {

		bg.sharedMaterial.SetColor("_LeftColor", coloring.GetColor(1));
		bg.sharedMaterial.SetColor("_RightColor", coloring.GetColor(0));

		var main = effMovingStars.main;
		main.startColor = coloring.GetColor(3);

		for (var i = 0; i < planets.Length; ++i) {
			planets[i].color = coloring.GetColor(2);
		}
	}
}
