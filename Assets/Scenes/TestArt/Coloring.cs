﻿using UnityEngine;

[CreateAssetMenu(menuName = "redshift/Coloring")]
public class Coloring : ScriptableObject {
	public int colorNum {get {return colors.Length;}}
	public Color GetColor(int idx) {
		return colors[idx];
	}

	[SerializeField] Color[] colors;
}
